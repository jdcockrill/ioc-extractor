import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { IocExtractorComponent } from './ioc-extractor/ioc-extractor.component';
import { HelpComponent } from './help/help.component';
import { NotFoundComponent } from './not-found/not-found.component';

/*
 TODO: componentise:
 - main page
 - help page
*/
const appRoutes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: IocExtractorComponent },
    { path: 'help', component: HelpComponent },
    { path: '**', component: NotFoundComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
