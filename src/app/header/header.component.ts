import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/take';
import * as csv from 'csv-stringify';

import * as fromApp from '../store/app.reducers';
import { validStates } from '../shared/ioc-extractor.quill-ext';
import { NativeBrowserService } from '../shared/native-browser.service';
import * as fromIoc from '../ioc-extractor/store/ioc-extractor.reducers';
import * as IocExtractorActions from '../ioc-extractor/store/ioc-extractor.actions';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  @ViewChild('fileForm')
  fileForm: NgForm;

  loadedFile: File;
  isCollapsed = true;

  constructor(private store: Store<fromApp.AppState>,
    public router: Router,
    private nativeBrowser: NativeBrowserService) { }

  /**
   * When the 'change' event fires for the file input, load the contents and emit an event with the string contents
   * @param event the change event indicating the user selected a file
   */
  onFileLoad(event: Event) {
    const htmlInput = (<HTMLInputElement>event.target);
    // Only one file input in the form
    const fileIn = htmlInput.files.item(0);
    this.loadFile(fileIn);
  }

  loadFile(fileIn: File) {
    const reader = new FileReader();

    // Setup an onload event which fires when the file has been loaded and emit an Angular event with the file string contents
    reader.onload = (newevent) => {
      this.store.dispatch(new IocExtractorActions.ProcessFile(reader.result));
      // this.fileLoad.emit(reader.result);
    };

    // Read the file in as text, which fires the above 'onload' event
    reader.readAsText(fileIn);
    // Keep a local copy in case of Reload (should probably move into NgRx state)
    this.loadedFile = fileIn;
  }

  /**
   * Accessor method that returns an Observable to the current value of
   * the 'fileLoaded' state member
   */
  isFileLoaded() {
    return this.store.select('iocExtractor').map((state: fromIoc.State) => {
      return state.fileLoaded;
    });
  }

  /**
   * Export the highlighted contents of the editor to a CSV file.
   * The first column is the type of entry (e.g. email, IP, etc)
   * The second column is the value.
   */
  onExportAsCsv() {
    const validStateNames = validStates.map(state => state.name);

    this.store.select('iocExtractor').take(1).map(
      (state: fromIoc.State) => {
        /*
        Get the latest content from Quill and identify the elements that have
        been higlighted with the custom styles we set for each data type, then
        return those in a new array which can be turned into a CSV
        */
        const values = [];
        const contents = state.quill.getContents();
        contents.forEach(op => {
          if (op.hasOwnProperty('insert') && op.hasOwnProperty('attributes')) {
            Object.keys(op.attributes).forEach(key => {
              if (validStateNames.includes(key)) {
                values.push([key, op.insert]);
              }
            });
          }
        });
        return values;
      }
    ).subscribe(
      vals => {
        /*
        Now get references to the browser document and window,
        convert the data to CSV, generate a download link and click it
        */
        const __window = this.nativeBrowser.getNativeWindow();
        const __document = this.nativeBrowser.getNativeDocument();

        csv(vals, (err, data) => {
          // create a Blob of the data we want to download
          // and generate a URL for it
          const csvBlob = new Blob([data], { 'type': 'text/csv' });
          const url = __window.URL.createObjectURL(csvBlob);

          // generate download link and set the href to the Blob that wraps the data
          // we want to download
          const downloadLink = __document.createElement('a');
          downloadLink.href = url;
          downloadLink.hidden = true;

          // generate a download filename based on the filename we originally loaded
          let fname;
          if (!this.loadedFile) {
            fname = 'ioc-data.csv';
          } else if (this.loadedFile.name.indexOf('.') > 0) {
            fname = this.loadedFile.name.substring(0, this.loadedFile.name.lastIndexOf('.')) + '.csv';
          } else {
            fname = this.loadedFile.name + '.csv';
          }
          downloadLink.download = fname;

          // click the link to trigger the download
          __document.body.appendChild(downloadLink);
          downloadLink.click();
          __document.body.removeChild(downloadLink);
        });
      }
    );
  }

  onExportAsStix() {

  }

  onReload() {
    this.loadFile(this.loadedFile);
  }

  hasContent(): Observable<boolean> {
    // is the content length greater than zero
    return this.store.select('iocExtractor').take(1).map(state => {
      // this might run before quill initialises.
      if (state.quill) {
        // it's initalised with a \n, so need to test > 1
        return state.quill.getText().length > 1;
      }
      return false;
    });
  }

  onReloadContents() {
    this.store.dispatch(new IocExtractorActions.ProcessContents());
  }

  onClose() {
    this.fileForm.resetForm();
    this.loadedFile = null;
    // this.fileForm.form.controls;
    this.store.dispatch(new IocExtractorActions.CloseFile());
  }
}
