import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { StoreModule } from '@ngrx/store';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { QuillModule } from 'ngx-quill';

import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HelpComponent } from './help/help.component';
import { IocExtractorComponent } from './ioc-extractor/ioc-extractor.component';
import { AppRoutingModule } from './app-routing.module';
import { reducers } from './store/app.reducers';
import { IocExtractorEffects } from './ioc-extractor/store/ioc-extractor.effects';
import { NativeBrowserService } from './shared/native-browser.service';
import { TruncateFilenamePipe } from './shared/truncatefilename.pipe';
import { MarkedDirective } from './shared/marked.directive';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HelpComponent,
    IocExtractorComponent,
    TruncateFilenamePipe,
    MarkedDirective,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    FormsModule,
    QuillModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([IocExtractorEffects]),
    StoreRouterConnectingModule,
    !environment.production ? StoreDevtoolsModule.instrument() : []
  ],
  providers: [NativeBrowserService],
  bootstrap: [AppComponent]
})
export class AppModule {}
