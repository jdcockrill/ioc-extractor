import * as IocExtractorActions from './ioc-extractor.actions';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';

import Quill, * as QuillNamespace from 'quill';
const QuillImport: any = QuillNamespace;
const Parchment = QuillImport.import('parchment');

import { UrlAttributor, IpAttributor, } from '../../shared/ioc-extractor.quill-ext';
import {
    extractIpv4Addresses,
    extractEmailAddresses,
    extractUrl,
    formatIpv4,
    formatEmail,
    formatUrl,
    formatHostname
} from '../ioc-extractor.service';

export interface State {
    quill: Quill;
    fileLoaded: boolean;
}

const initialState: State = {
    quill: null,
    fileLoaded: false
};

export function iocExtractorReducer(state = initialState,
    action: IocExtractorActions.IocExtractorActions) {
    switch (action.type) {
        case IocExtractorActions.FILE_LOADED:
            const fileWasLoaded = true;
        // tslint:disable-next-line:no-switch-case-fall-through
        case IocExtractorActions.INIT_QUILL:
            const initQuill = action.payload;
            return {
                ...state,
                quill: initQuill,
                fileLoaded: state.fileLoaded || fileWasLoaded
            };
        case IocExtractorActions.FORMAT_IP:
            const ipQuill = formatIpv4(state.quill);
            return {
                ...state,
                quill: ipQuill
            };
        case IocExtractorActions.FORMAT_URL:
            const urlQuill = formatUrl(state.quill);
            return {
                ...state,
                quill: urlQuill
            };
        case IocExtractorActions.FORMAT_EMAIL:
            const emailQuill = formatEmail(state.quill);
            return {
                ...state,
                quill: urlQuill
            };
        case IocExtractorActions.FORMAT_HOSTNAME:
            const hostnameQuill = formatHostname(state.quill);
            return {
                ...state,
                quill: hostnameQuill
            };
        case IocExtractorActions.CLOSE_FILE:
            const closeQuill = state.quill;
            closeQuill.setText('');
            return {
                ...state,
                quill: closeQuill,
                fileLoaded: false
            };
        default:
            return state;
    }
}
