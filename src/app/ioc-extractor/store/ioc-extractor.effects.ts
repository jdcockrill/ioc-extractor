import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Quill } from 'quill';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

import * as fromApp from '../../store/app.reducers';
import * as fromIoc from './ioc-extractor.reducers';
import * as IocExtractorActions from './ioc-extractor.actions';
import { extractIpv4Addresses, extractEmailAddresses, extractUrl, extractHostname } from '../ioc-extractor.service';

@Injectable()
export class IocExtractorEffects {

    actions: Function[] = [extractIpv4Addresses, extractEmailAddresses];

    @Effect()
    processFile = this.action$
        .ofType(IocExtractorActions.PROCESS_FILE)
        .withLatestFrom(this.store.select('iocExtractor'))
        .map(([action, state]) => {
            const thisQuill = state.quill;
            const fileContents = (<IocExtractorActions.ProcessFile>action).payload;
            thisQuill.setText(fileContents);
            return thisQuill;
        })
        // TODO: it'd be nicer if the 'extract' methods were an array
        // and we somehow mapped those onto quill so that they happened
        // in parallel.
        .map((quill: Quill) => {
            return extractIpv4Addresses(quill);
        })
        .map((quill: Quill) => {
            return extractEmailAddresses(quill);
        })
        .map((quill: Quill) => {
            return extractUrl(quill);
        })
        .map((quill: Quill) => {
            return extractHostname(quill);
        })
        .map((quill: Quill) => {
            return {
                type: IocExtractorActions.FILE_LOADED,
                payload: quill
            };
        });

    @Effect()
    processContents = this.action$
        .ofType(IocExtractorActions.PROCESS_CONTENTS)
        .withLatestFrom(this.store.select('iocExtractor'))
        .map(([action, state]) => {
            const thisQuill = state.quill;
            // don't need to change the contents
            return thisQuill;
        })
        .map((quill: Quill) => {
            return extractIpv4Addresses(quill);
        })
        .map((quill: Quill) => {
            return extractEmailAddresses(quill);
        })
        .map((quill: Quill) => {
            return extractUrl(quill);
        })
        .map((quill: Quill) => {
            return extractHostname(quill);
        })
        .map((quill: Quill) => {
            return {
                type: IocExtractorActions.INIT_QUILL,
                payload: quill
            };
        });

    constructor(private action$: Actions, private store: Store<fromApp.AppState>) { }
}
