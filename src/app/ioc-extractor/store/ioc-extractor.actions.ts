import { Action } from '@ngrx/store';
import { Quill } from 'quill';

export const INIT_QUILL = 'INIT_QUILL';
export const FILE_LOADED = 'FILE_LOADED';
export const PROCESS_FILE = 'PROCESS_FILE';
export const PROCESS_CONTENTS = 'PROCESS_CONTENTS';
export const FORMAT_IP = 'FORMAT_IP';
export const FORMAT_URL = 'FORMAT_URL';
export const FORMAT_HOSTNAME = 'FORMAT_HOSTNAME';
export const FORMAT_EMAIL = 'FORMAT_EMAIL';
// todo
export const CLOSE_FILE = 'CLOSE_FILE';

export class InitQuill implements Action {
    readonly type = INIT_QUILL;

    /**
     * @param payload the initialised Quill instance
     */
    constructor(public payload: Quill) { }
}

export class FileLoaded implements Action {
    readonly type = FILE_LOADED;
    /**
     * @param payload the initialised Quill instance
     */
    constructor(public payload: Quill) { }
}

export class ProcessFile implements Action {
    readonly type = PROCESS_FILE;
    /**
     * Constructor for FileLoaded Action
     * @param payload the string contents of the file
     */
    constructor(public payload: string) {}
}

export class ProcessContents implements Action {
    readonly type = PROCESS_CONTENTS;
}

export class FormatIp implements Action {
    readonly type = FORMAT_IP;
}

export class FormatUrl implements Action {
    readonly type = FORMAT_URL;
}

export class FormatEmail implements Action {
    readonly type = FORMAT_EMAIL;
}

export class FormatHostname implements Action {
    readonly type = FORMAT_HOSTNAME;
}

export class CloseFile implements Action {
    readonly type = CLOSE_FILE;
}

export type IocExtractorActions = InitQuill
    | FileLoaded
    | ProcessFile
    | ProcessContents
    | FormatIp
    | FormatUrl
    | FormatEmail
    | FormatHostname
    | CloseFile;
