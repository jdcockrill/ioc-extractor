import { Component, OnInit, ViewChild } from '@angular/core';
import Quill, * as QuillNamespace from 'quill';
const QuillImport: any = QuillNamespace;
const Parchment = QuillImport.import('parchment');
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as fromApp from '../store/app.reducers';
import * as fromIoc from './store/ioc-extractor.reducers';
import * as IocExtractorActions from './store/ioc-extractor.actions';

@Component({
  selector: 'app-ioc-extractor',
  templateUrl: './ioc-extractor.component.html',
  styleUrls: ['./ioc-extractor.component.css']
})
export class IocExtractorComponent implements OnInit {

  quillModules = {
    toolbar: {
      handlers: {
        'ip': () => { this.store.dispatch(new IocExtractorActions.FormatIp()); },
        'url': () => { this.store.dispatch(new IocExtractorActions.FormatUrl()); },
        'email': () => { this.store.dispatch(new IocExtractorActions.FormatEmail()); },
        'hostname': () => { this.store.dispatch(new IocExtractorActions.FormatHostname()); }
      }
    }/* TODO: come back to keyboard bindings
    keyboard: {
      bindings: {
        'italic': { // IP
          key: 'K',
          shortKey: true,
          collapsed: false,
          handler: this.generateDispatcher(new IocExtractorActions.FormatIp())
        },
        'underline': { // URL
          key: 'L',
          shortKey: true,
          collapsed: false,
          handler: this.generateDispatcher(new IocExtractorActions.FormatUrl())
        },
        'email': { // URL
          key: 'M',
          shortKey: true,
          collapsed: false,
          handler: this.generateDispatcher(new IocExtractorActions.FormatEmail())
        },
        'hostname': { // URL
          key: 'D',
          shortKey: true,
          collapsed: false,
          handler: this.generateDispatcher(new IocExtractorActions.FormatEmail())
        }
      }
    }*/
  };

  constructor(private store: Store<fromApp.AppState>) { }

  ngOnInit() { }

  /**
   * Dispatch the configured instance of Quill to the NgRx store
   * @param quill the local instance of Quill that has been configured
   */
  onEditorReady(quill: Quill) {
    this.store.dispatch(new IocExtractorActions.InitQuill(quill));
  }
  generateDispatcher(actionToDispatch) {
    const storez = this.store;
    return (range, context) => {
      storez.dispatch(actionToDispatch);
      // prevents other actions from happening (e.g. italics)
      return true;
    };
  }
}
