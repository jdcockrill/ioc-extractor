import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TruncateFilenamePipe } from './truncatefilename.pipe';

describe('TruncateFilenamePipe', () => {
  let component: TruncateFilenamePipe;

  it('should not truncate anything less than the `limit` parameter', () => {
    component = new TruncateFilenamePipe();
    const testVal = '12characters';
    expect(component.transform(testVal, 13)).toBe(testVal);
  });

  it('should truncate anything more than the `limit` parameter, without a file extension \
      or trail, to remove the end', () => {
    component = new TruncateFilenamePipe();
    const testVal = '12characters';
    expect(component.transform(testVal, 10)).toBe('12characte');
  });

  it('where a file extension exists but no trail is specified, should truncate anything \
      more than the `limit` parameter by removing characters from the end', () => {
      component = new TruncateFilenamePipe();
      const testVal = '12characters.txt';
      expect(component.transform(testVal, 12)).toBe('12characters');
      expect(component.transform(testVal, 12).length).toBe(12);
    });

  it('where a file extension exists and trail is specified, should truncate the prefix to \
      make the whole string the length of the `limit` parameter. The trail will be in the\
      before the ext', () => {
      component = new TruncateFilenamePipe();
      const testVal = '12characters.txt';
      expect(component.transform(testVal, 12, '...')).toBe('12char...txt');
      expect(component.transform(testVal, 12, '...').length).toBe(12);
      // if it was at the end indexOf would return 9 because the trail is of length 3.
      expect(component.transform(testVal, 12, '...').indexOf('...') < 9).toBeTruthy();
    });

  it('where there is no file extension and trail is specified, should truncate \
      the whole string to the length of the `limit` parameter and put the trail \
      on the end', () => {
      component = new TruncateFilenamePipe();
      const testVal = '12characters';
      expect(component.transform(testVal, 10, '...')).toBe('12chara...');
      expect(component.transform(testVal, 10, '...').length).toBe(10);
      expect(component.transform(testVal, 10, '...').endsWith('...')).toBeTruthy();
    });
});
