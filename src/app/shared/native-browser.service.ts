/**
 * This class exists to expose browser native objects.
 * This pattern of injecting a service to wrap these objects
 * is to make it so that dynamically replacing them in other
 * circumstances, such as not running in a browser, is possible
 */

function _window(): Window {
   // return the global native browser window object
   return window;
}

function _document(): Document {
    // return the global native browser document object
    return document;
}

export class NativeBrowserService {
   getNativeWindow(): Window {
      return _window();
   }

   getNativeDocument(): Document {
       return _document();
   }
}
