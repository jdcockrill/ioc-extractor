
import * as QuillNamespace from 'quill';
const QuillImport: any = QuillNamespace;
const Parchment = QuillImport.import('parchment');

/**
 * This file contains a set of custom Attributors for Parchment which will be used
 * to decorate parts of the document to identify them as IP addresses or CSVs or similar.
 */
export const IpAttributor = new Parchment.Attributor.Class('ip', 'ioc-ip', { scope: Parchment.Scope.INLINE });
export const UrlAttributor = new Parchment.Attributor.Class('url', 'ioc-url', { scope: Parchment.Scope.INLINE });
export const EmailAttributor = new Parchment.Attributor.Class('email', 'ioc-email', { scope: Parchment.Scope.INLINE });
export const HostnameAttributor = new Parchment.Attributor.Class('hostname', 'ioc-hostname', { scope: Parchment.Scope.INLINE });

export interface CssStyle {
    name: string;
    value: string;
}

export const ipv4State: CssStyle = { name: 'ip', value: 'true' };
export const emailState: CssStyle = { name: 'email', value: 'true' };
export const urlState: CssStyle = { name: 'url', value: 'true' };
export const hostnameState: CssStyle = { name: 'hostname', value: 'true' };

export const validStates: CssStyle[] = [ipv4State, emailState, urlState, hostnameState];
