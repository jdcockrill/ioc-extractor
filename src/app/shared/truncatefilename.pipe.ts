import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'truncatefilename'
})

export class TruncateFilenamePipe implements PipeTransform {

    transform(value: string, limit: number, trail?: string): string {
        // if there's a file extension, preserve it, unless that alone takes us over the limit
        const extStart = value.lastIndexOf('.');
        if (extStart > 0 && trail) {
            if ((value.length - extStart) > limit) {
                // pass - extension is longer than limit
            } else {
                // preserve the extension
                const prefix = value.substring(0, extStart);
                const ext = value.substring(extStart + 1, value.length); // just the extension, not the '.'
                const charsToRemove = value.length + trail.length - limit - 1; // -1 for the '.' we removed
                const newPrefix = prefix.substring(0, prefix.length - charsToRemove) + trail;
                return newPrefix + ext;
            }
        }
        const thisTrail = typeof trail !== 'undefined' ? trail : ''; // if trail is undefined, use '' instead.
        return value.length > limit ? value.substring(0, limit - thisTrail.length) + thisTrail : value;
    }
}
