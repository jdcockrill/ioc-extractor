import { ActionReducerMap } from '@ngrx/store';
import * as fromIocExtractor from '../ioc-extractor/store/ioc-extractor.reducers';

export interface AppState {
    iocExtractor: fromIocExtractor.State;
    // auth: fromAuthState.State
}

export const reducers: ActionReducerMap<AppState> = {
    iocExtractor: fromIocExtractor.iocExtractorReducer,
    // auth: fromAuthState.authReducer
};
