import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as QuillNamespace from 'quill';
import { UrlAttributor, IpAttributor, EmailAttributor, HostnameAttributor, } from './shared/ioc-extractor.quill-ext';
const QuillImport: any = QuillNamespace;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  ngOnInit() {
    this.registerCustomAttributors();
  }

  /**
   * Register the custom extensions for Quill that are used
   * to identify the tagged elements of the editor content.
   * Needs to be done here so they are ready for when Quill
   * itself is initialised.
   */
  registerCustomAttributors() {
    QuillImport.register(IpAttributor, false);
    QuillImport.register(UrlAttributor, false);
    QuillImport.register(EmailAttributor, false);
    QuillImport.register(HostnameAttributor, false);
  }
}
