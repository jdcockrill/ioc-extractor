import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: `
<h2>
  <i class="fas fa-exclamation-triangle text-danger"></i> Page Not Found!
</h2>
`
})
export class NotFoundComponent {

  constructor() { }

}
