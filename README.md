# IocExtractor

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.4.

Inspired by the work of Stephen Brannon: https://github.com/stephenbrannon/IOCextractor.

IOC Extractor exists to help extract [Indicators of Compromise](https://en.wikipedia.org/wiki/Indicator_of_compromise)
from text documents. Upon loading a file, the contents will be compared against a set of regular expressions 
to attempt to auto-identify entities such as hostnames, IP addresses, e-mail addresses and URLs. These IOCs 
can then be exported as CSV.

## Development

Clone the project and run `ng serve` to run locally. See [Angular Development](#angular-development) section below for more information.

## Features

* Load files into an editor in the browser (not to a server) and auto-identify the following Indicator types:
  * IP Addresses
  * E-mail addresses
  * URLs
  * Hostnames
* Enter your own text (e.g. copy-paste into editor) and optionally run the same auto-processing
* Export the IOCs to CSV

## Todo

* Add keybindings that allow users to add/clear highlights
* Add a 'load without processing' button
* Add export to other formats such as STIX (find more) - https://oasis-open.github.io/cti-documentation/stix/walkthrough
* Add an optional 'preview' of a selected output type - maybe as an accordian view
* (Internal) move the file reference out of header.component.ts and into the store. Previously it seemed OK as it just helped with UI logic, but actually I think it needs to go in the main state.

<hr>

# Angular Development

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
